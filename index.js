// import des modules
const express = require('express');
const connection = require('./config/db');
const cors = require('cors');
const bcrypt = require('bcrypt');

// Déclaration de l'objet app qui permet de gérer les req & res du serveur vers le client
const app = express();

// Définition du port 
const port = 3000;

// Configure l'app pour parser les requetes au format JSON
app.use(
    express.json()
);

// Configure l'app pour parser les requetes au format URL
app.use(express.urlencoded({
    extended: true
}));

// Configuration du module CORS pour autoriser une origine spécifique
app.use(cors({
    origin: 'http://localhost:8080'
}));

// Endpoint register pour créer un nouvel utilisateur
app.post('/register', (req, res) => {
    const { email, password } = req.body;
    // Vérifie que les champs sont bien remplis
    if (!email || !password) {
        res.status(400)
            .json({ error: `L'email et le password sont des champs obligatoires` });
    } else {
        // Hashe le mot de passe avec SaltRound défini à 10 
        bcrypt.hash(password, 10, function (bcryptError, hashedPassword) {
            if (bcryptError) {
                res.status(500)
                    .json({ error: bcryptError.message });
            } else {
                // création du nouvel utilisateur
                connection.execute('INSERT INTO user (email, password) VALUES (?, ?)',
                    [email, hashedPassword],
                    (mysqlErr, results) => {
                        if (mysqlErr) {
                            res.status(500)
                                .json({ error: mysqlErr.message });
                        } else {
                            res.status(201)
                                .json({ user_id: results.insertId, email: email })
                        }
                    });
            }
        });
    }
});

// Endpoint /login pour gérer les connexions
app.post('/login', (req, res) => {
    const { email, password } = req.body;
    // Vérifie que les champs sont bien remplis
    if (!email || !password) {
        res.status(400)
            .json({ error: `L'email et la mot de passe sont obligatoires` });
    } else {
        // Vérifie l'existence de l'utilisateur grâce à son adresse mail
        connection.query('SELECT * FROM user WHERE email = ?',
            [email],
            (mysqlError, results) => {
                if (mysqlError) {
                    res.status(500)
                        .json({ error: mysqlError.message });
                } else if (results.length === 0) {
                    res.status(404)
                        .json({ error: `l'email est invalide` });
                } else {
                    // Compare les mots de passe 
                    const user = results[0];
                    const hashedPassword = user.password;
                    bcrypt.compare(password, hashedPassword, function (bcryptError, passwordMatch) {
                        if (bcryptError) {
                            res.status(500)
                                .json({ error: bcryptError.message });
                        } else if (passwordMatch) {
                            res.status(200)
                                .json(`L'utilisateur ID ${user.user_id} avec l'email ${user.email} est connecté`);
                        } else {
                            res.status(400)
                                .json({ error: 'Mot de passe invalide' })
                        }
                    });
                }
            });
    }
})


// démarrage du serveur sur le port choisi
app.listen(port, () => {
    console.log(`app listening on port ${port}, http://localhost:${port}/`)
})