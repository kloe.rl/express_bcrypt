//import du module mysql2
const mysql = require('mysql2')
// 
require('dotenv').config();

// configuration et création d'un pool de connexion à la BDD
const connection = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD
});

// Etablissement de la connexion
connection.getConnection((err) => {
    if (err instanceof Error) {
        console.log('getConnection error:', err);
        return;
    }
});

// export du module
module.exports = connection;